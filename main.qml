import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 1024
    height: 800
    title: qsTr("Hello World")

    ListView {
            id: navColumn
            height: parent.height
            width: 300
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 2

            clip: true
    
            model: groups
    
            delegate: groupsDelegate
    
            section.property: "type"
            section.delegate: groupsSectionDelegate

            highlight: Rectangle { color: "lightsteelblue"; radius: 5 }

        }
    
    Component {
           id: groupsDelegate
   
           Item {
               width: ListView.view.width
               height: 40
               Column {
                   Text { text: '<b>' + title + '</b>' }
                   Text { text: groupid }
               }

               MouseArea {
                   id: mouse_area1
                   z: 1
                   hoverEnabled: false
                   anchors.fill: parent
                   onClicked: {
                       navColumn.currentIndex = index
                   }
               }
           }


       }

    Component {
           id: groupsSectionDelegate

           BlueBox {
               width: ListView.view.width
               height: 20
               //text: section
               fontColor: '#e0e0e0'

               Column {
                   Text { text: section + " (" + section.count + ")"}
               }
           }
       }

    ListModel {
        id: groups

        ListElement {type: "group"; title: "Work Friends"; groupid: "710829b408e8b368273cbc20b07f1c0d"}
        ListElement {type: "group"; title: "Cwtch Dev"; groupid: "a219b9740fc76367833cbc20b07d1cee" }
        ListElement {type: "group"; title: "Gaming Squad"; groupid: "930829b408e8b364563cbc20b07a6560" }

        ListElement {type: "bulletin"; title: "Vancouver Listings"; groupid: "890a47403e87368273cbc2ebf7f1cdc"}
        ListElement {type: "bulletin"; title: "Game Discussions"; groupid: "cc45892408123879273ec2a435cc4234"}
    }

    ListModel {
        id: leafStructure

        ListElement { title: "Community";
            count: 3
            subItems: [
                ListElement { title: "activities" },
                ListElement { title: "artists" },
                ListElement { title: "childcare" }
            ]
        }
        ListElement { title: "Services";
            count: 4
            subItems: [
                ListElement { title: "automotive" },
                ListElement { title: "beauty" },
                ListElement { title: "cell/mobile" },
                ListElement { title: "computer" }
            ]
        }
    }


    ListView {
            id: content
            height: parent.height
            width: parent.width - 300
            anchors.top: parent.top
            anchors.left: navColumn.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 2
            anchors.leftMargin: 20

            clip: true

            model: leafStructure

            delegate: sectionDelegate
        }


    Component {
        id: sectionDelegate

        Column {
            height: 200

            Text {
                text: title  + " (" + count  + ")"
                font.bold: true
            }

            Loader {
                id: subItemLoader

                visible: true

                property variant subItemModel : subItems
                sourceComponent: subSectionDelegate
            }
        }


    }

    Component {
        id: subSectionDelegate

        Rectangle {
            border.color: "red"
            border.width: 2
            width: 60
            height: 100

            Text { text: "foo" }


        Column {
            property alias model : subItemRepeater.model
            width: 200

            Repeater {
                id: subItemRepeater
                delegate: Rectangle {
                    color: "#cccccc"
                    height: 40
                    width: 200
                    border.color: "black"
                    border.width: 2

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        x: 30
                        font.pixelSize: 18
                        text: title
                    }
                }
            }
        }
        }
    }


}
